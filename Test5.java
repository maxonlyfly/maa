package rsssss;

//Creating interface that has 4 methods  
interface A{  
	void a();//bydefault, public and abstract  
	void b();  
	void c();  
	void d();
} 

abstract class Bike {  
//	   Bike(){System.out.println("bike is created");}  
//	   abstract void run();  
	   void changeGear(){System.out.println("gear changed");}  

}

class honda extends Bike{
	void run(){System.out.println("running safely..");}
}

//Creating a test class that calls the methods of A interface  
class Test5 extends honda {  

	public static void main(String args[]){ 
		Test5 obj = new Test5();  
		Bike b = new honda();
		 b.run();  
		  b.changeGear();  
	}}